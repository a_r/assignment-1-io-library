section .text

%define SYSCALL_EXIT 60
%define SYSCALL_READ 0
%define SYSCALL_WRITE 1
%define STDOUT 1
%define STDIN 0


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYSCALL_EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax 		; rax = 0
	.loop:
	cmp byte [rdi+rax], 0	; compare char with zero
	je .end
	inc rax ; add 1 char
	jmp .loop
	.end:
	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi  ; save rdi before calling the func
	call string_length
	pop rsi   ; restore address_string to rsi
	mov rdx, rax   ; rdx = string length
	mov rax, 1
	mov rdi, 1
	syscall
	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA         ; ASCII-код символа новой строки


; Принимает код символа и выводит его в stdout
print_char:
	push rdi		; char to stack
	mov rsi, rsp		; rsi = address of char 
	mov rax, SYSCALL_WRITE	
	mov rdx, 1          ; rdx = char length
	mov rdi, STDOUT
	syscall
	pop rdi			; restore rdi
	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	mov rax, rdi
	test rdi, rdi		
	jns print_uint
	push rdi
	mov rdi, '-'
	call print_char		; print minus
	pop rdi
	neg rdi			; change sign


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push rbp
	mov rax, rdi 		
	mov rbp, rsp    	; rsi = initial address of stack
	push 0     		; for print_string
	mov r8, 10    		; divider
	.loop: 
	  xor rdx, rdx 		
	  div r8		; rax /= r8, rdx %= r8
	  add dl , '0'   	; into ASCII-code
	  dec rsp 		; for number 
	  mov byte[rsp], dl 	
	  test rax, rax    	; if rax div 10 then loop else next instr
	  jnz .loop  
	  
	mov rdi, rsp 	    		
	call print_string 
	mov rsp, rbp
	pop rbp
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
       xor rax, rax
       .loop:
	 mov r9b, byte [rdi]	; r9b = char 0f first string
	 cmp byte [rsi], r9b	; compare chars of srting1 and string2
	 jne .noteq
	 test r9b, r9b		; check for 0 byte at the end
	 je .eq
	 inc rdi		; add 1 char of string1
	 inc rsi		; add 1 char of string2
	 jmp .loop
       .eq:
	 inc rax
	 ret
       .noteq:
	  ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:  
	mov rax, SYSCALL_READ 		
	mov rdi, STDIN 		
	push 0			; char to stack 
	mov rsi, rsp 		; rsi = address of char
	mov rdx, 1 		; rdx = char length 
	syscall 	
	mov rax, [rsp]		
	pop r9    		; restore rsp
	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word: 
	push r12 		;save calee-saved registers 
    	push r13 
    	push r14 
    	mov r12, rdi 		; buffer address 
	mov r13, rsi    	; buffer size 
	xor r14, r14    	; clear counter of chars 
 
    .space_is_evil: 
        call read_char 
        cmp rax, ' '		 ; check_space
        je .space_is_evil
        cmp rax, `\t` 		 ; check_tab 
        je .space_is_evil
        cmp rax,  `\n` 		 ; check_new_line
        je .space_is_evil
        
    .read: 
        cmp r14, r13 		 ; if word > buffer_size then error
        jge .error
        cmp al, 0 
        je .success 
        cmp al, ' ' 
        je .success 
        cmp al, `\t` 
        je .success 
        cmp al, `\n` 
        je .success 
 	
        mov byte [r12 + r14], al  ; save chars to buffer 
        inc r14 
        call read_char 
        jmp .read 
        
    .success: 
        mov byte [r12 + r14], 0   ;add 0 for null-terminated 
	mov rax, r12 		  ; pointer for begining of word
	mov rdx, r14 		  ; pointer for word length
        jmp .end
	
    .error: 
        xor rax, rax 
        pop r14 
        pop r13 
        pop r12 
        ret
	
    .end: 
        pop r14    		;restore calee-saved registers 
        pop r13 
        pop r12 
        ret
	
	

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
      xor rax, rax		; result
      xor rdx, rdx		; count_length
      .loop:
	cmp byte [rdi+rdx], '0'	; compare first char with 0
	jb .end			; if not number then end
	cmp byte [rdi+rdx], '9' 
	ja .end
	
	xor r9, r9
	mov r9b, byte [rdi+rdx]	
	inc rdx
	imul rax, 10		; muiltiply char * 10
	sub r9b, '0'		; convert char into num
	add rax, r9		; add to current chars
	jmp .loop
      .end: 
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int: 
	cmp byte[rdi], '-'	    ; compare char_int with minus
	je .negative    
	cmp byte[rdi], '+'	    ; compare char_int with plus
	je .positive
	jne parse_uint
      .negative: 
	inc rdi 
      .positive:
	call parse_uint 
	test rdx, rdx		    ; if word=0 then error
	jz .error
	neg rax 
	inc rdx
	ret 
      .error:
        mov rax, 0
	ret
	


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax                ; count_length
    .loop:
        cmp rax, rdx            ; compare count_length with initial string_length
        jge .end                
        mov r9b, byte [rdi+rax] 
        mov byte [rsi+rax], r9b  
        test r9b, r9b
        jz .success             
        inc rax                 
        jmp .loop               
    .end:
        xor rax, rax            
    .success:
        ret
   
